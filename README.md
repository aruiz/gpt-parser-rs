# GPT Parser Library

![Crates.io](https://img.shields.io/crates/v/gpt-parser)
![MIT License](https://img.shields.io/crates/l/gpt-parser)
![Rust Version](https://img.shields.io/badge/rust-1.53+-blue.svg)
![Platform](https://img.shields.io/badge/platform-linux%20%7C%20windows%20%7C%20macos%20%7C%20embedded-lightgrey.svg)

**gpt-parser** is a Rust library for parsing GPT (GUID Partition Table) partition tables as defined by the UEFI standard. It provides an endian-aware, read-only parser that can be used in both standard and embedded Rust environments. This means you can use it on resource-constrained systems where the standard library is not available.

## Features

- Parsing GPT partition tables.
- Validating GPT header signatures.
- CRC32 checksum verification of the partition table.
- Handling disks with different block sizes (e.g., 512 bytes or 4K).
- Extracting information about partitions, including their UUIDs and attributes.
- Supports `no_std`

## Usage

To use this library in your Rust project, add it as a dependency in your `Cargo.toml`:

```toml
[dependencies]
gpt-parser = "*"
```

### Standard Rust Environment

In a standard Rust environment (with the standard library), you can use the library as follows:

```rust
extern crate gpt_parser;

use gpt_parser::{GPTHeader, LBA512};

fn main() {
    // Replace 'disk_image' with your disk image data.
    let disk_image: &[u8] = /* Load your disk image here */;

    // Create an LBA512 instance from your disk image.
    let lba = LBA512::from_slice(&disk_image);

    // Parse the GPT header.
    let gpt_header = match GPTHeader::parse_gpt_header(&lba) {
        Ok(header) => header,
        Err(err) => {
            eprintln!("Error parsing GPT header: {}", err);
            return;
        }
    };

    // Access information about the partitions.
    let partitions = gpt_header.get_partitions(&lba);

    for partition in partitions {
        println!("Partition Type: {:?}", partition.part_type);
        println!("Partition UUID: {:?}", partition.id);
        println!("First LBA: {:?}", partition.first_lba);
        println!("Last LBA: {:?}", partition.last_lba);
        println!("Attributes: {:?}", partition.attr);
        println!("Name: {:?}", partition.name);
    }
}
```

Replace `'disk_image'` with the binary data of your GPT-formatted disk image.

### Embedded Rust Environment

If you want to use this library in an embedded Rust environment (no standard library), enable the `no_std` feature in your project's `Cargo.toml`:

```toml
[dependencies]
gpt-parser = { version = "*", features = ["no_std"] }
```

In an embedded environment, you may need to provide your own implementation for certain functionality (e.g., memory allocation) as required by your specific platform.

## Testing

This library includes a set of tests to ensure its correctness. To run the tests, use the following command:

```bash
cargo test
```

## License

This library is open-source and distributed under the terms of the MIT License.

## Author

This library was developed by Alberto Ruiz (aruiz@redhat.com).

## Contributions

Contributions to this project are welcome. Please submit issues and pull requests as needed.

## Acknowledgments

This library is based on the UEFI standard for GPT partition tables and uses various Rust libraries to achieve its functionality.

Enjoy using this library to parse GPT partition tables in your projects! If you encounter any issues or have suggestions for improvement, please don't hesitate to reach out.